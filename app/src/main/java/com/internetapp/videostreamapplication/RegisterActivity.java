package com.internetapp.videostreamapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

public class RegisterActivity extends Activity {
    private String siteIp;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            submitButtonClicked(v);
        }
    };

    private RequestAsyncInterface requestAsyncInterface = new RequestAsyncInterface() {
        @Override
        public void onAsyncTaskResult(int code, byte[] msg, int taskId) {
            processPostResult(code, msg, taskId);
        }
    };

    Pattern namePattern;
    Pattern emailPattern;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Intent intent = getIntent();
        siteIp = intent.getStringExtra(Constant.SITE_IP_ADDRESS);

        Button submitButton = findViewById(R.id.b_register_submit);
        submitButton.setOnClickListener(onClickListener);

        namePattern = Pattern.compile(Constant.NAME_PATTERN);
        emailPattern = Pattern.compile(Constant.EMAIL_PATTERN);
    }

    private void submitButtonClicked(View v) {
        String nameString = ((EditText)findViewById(R.id.et_register_name)).getText().toString();
        String emailString = ((EditText)findViewById(R.id.et_register_email)).getText().toString();
        String usernameString = ((EditText)findViewById(R.id.et_register_username)).getText().toString();
        String passwordString = ((EditText)findViewById(R.id.et_register_password)).getText().toString();
        String confirmString = ((EditText)findViewById(R.id.et_register_confirm)).getText().toString();

        if (!namePattern.matcher(nameString).matches()) {
            Toast.makeText(this, "Need to fill name field", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!emailPattern.matcher(emailString).matches()) {
            Toast.makeText(this, "Need to fill email field", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!namePattern.matcher(usernameString).matches()) {
            Toast.makeText(this, "Need to fill username field", Toast.LENGTH_SHORT).show();
            return;
        }

        if (passwordString.length() < 8 || 71 < passwordString.length()) {
            Toast.makeText(this, "Need to fill password field", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!passwordString.equals(confirmString)) {
            Toast.makeText(this, "The passwords do not match", Toast.LENGTH_SHORT).show();
            return;
        }

        String postParameters = "name=" + nameString +
                                "&email=" + emailString +
                                "&username=" + usernameString +
                                "&password=" + passwordString;

        SendPostAsyncTask sendPostAsyncTask = new SendPostAsyncTask(
                getSharedPreferences(Constant.SP_TOKEN_PREF, MODE_PRIVATE)
        );
        sendPostAsyncTask.setOnResultListener(requestAsyncInterface);
        sendPostAsyncTask.execute(
                siteIp + "/mobile/register",
                postParameters
        );

        v.setEnabled(false);
    }

    private void processPostResult(int code, byte[] msg, int taskId) {
        Button submitButton = findViewById(R.id.b_register_submit);
        submitButton.setEnabled(true);

        if (code < 200 || 299 < code) {
            Toast.makeText(this, new String(msg, StandardCharsets.UTF_8), Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(Constant.SITE_IP_ADDRESS, siteIp);
        startActivity(intent);
    }
}
