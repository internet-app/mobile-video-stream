package com.internetapp.videostreamapplication;

public interface RequestAsyncInterface {
    void onAsyncTaskResult(int code, byte[] msg, int taskId);
}
