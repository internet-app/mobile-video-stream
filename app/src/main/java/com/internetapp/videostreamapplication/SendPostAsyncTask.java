package com.internetapp.videostreamapplication;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import androidx.annotation.Nullable;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


public class SendPostAsyncTask extends AsyncTask<String, Void, Integer> {
    private RequestAsyncInterface listener;
    private byte[] resultMsg = null;
    private int taskId = 0;
    private SharedPreferences prefs;

    void setOnResultListener(RequestAsyncInterface listener) {
        this.listener = listener;
    }
    SendPostAsyncTask(SharedPreferences prefs) {
        this.prefs = prefs;
    }
    void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    @Override
    protected Integer doInBackground(String... params) {
        String requestURL = params[0];
        String urlParameters = params[1];

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;
        int resultCode = Constant.START_ERROR_CODE;

        try {
            URL url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setRequestProperty("Device-Type", "mobile");

            String cookieParameter = prefs.getString(Constant.SP_TOKEN_NAME, null);
            if (cookieParameter != null) {
                conn.setRequestProperty("Cookie", cookieParameter);
            }

            conn.setUseCaches(false);
            conn.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            resultCode = conn.getResponseCode();

            if (resultCode == HttpURLConnection.HTTP_OK) {
                resultMsg = toByteArray(conn.getContentLength(), conn.getInputStream());
                String cookieMsg = conn.getHeaderField("Set-Cookie");
                if (cookieMsg != null) {
                    String token = cookieMsg.substring(0, cookieMsg.indexOf(';'));
                    if (!token.isEmpty()) {
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(Constant.SP_TOKEN_NAME, token);
                        editor.apply();
                    }
                }
            } else if (resultCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                resultMsg = toByteArray(conn.getContentLength(), conn.getErrorStream());
                if ("The token does not found".equals(
                        new String(resultMsg, StandardCharsets.UTF_8))) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.remove(Constant.SP_TOKEN_NAME);
                    editor.apply();
                }
            }

            conn.disconnect();
        } catch (Exception e) {
            resultMsg = e.getMessage().getBytes();
        }

        return resultCode;
    }

    @Override
    protected void onPostExecute(Integer resultCode) {
        super.onPostExecute(resultCode);
        listener.onAsyncTaskResult(resultCode, resultMsg, taskId);
    }

    @Nullable
    private byte[] toByteArray(int expectedLength, InputStream is) {
        // 104857600 - Theoretical limit is 100 mb
        if (expectedLength > 104857600 || expectedLength < 1) {
            return null;
        }

        byte[] resultArray = new byte[expectedLength];
        int readedBytes = 0;
        int loopResult;
        do {
            try {
                loopResult = is.read(resultArray, readedBytes, expectedLength);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            readedBytes += loopResult;
            expectedLength -= loopResult;
        } while (loopResult != -1 && expectedLength != 0);

        return resultArray;
    }
}
