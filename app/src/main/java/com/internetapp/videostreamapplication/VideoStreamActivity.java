package com.internetapp.videostreamapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

public class VideoStreamActivity extends Activity {
    private HlsMediaSource hlsMediaSource;
    private SimpleExoPlayer exoPlayer;
    private int startWindow;
    private long startPosition;

    String videoURL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_stream);

        Intent intent = getIntent();
        String streamUrl = intent.getStringExtra(Constant.VIDEO_STREAM_URL);
        videoURL = streamUrl + "playlist.m3u8";

        PlayerView exoPlayerView = findViewById(R.id.exo_player_view);

        exoPlayer = ExoPlayerFactory.newSimpleInstance(this);
        exoPlayerView.setPlayer(exoPlayer);
        hlsMediaSource = buildHlsMediaSource(Uri.parse(videoURL));

        clearStartPosition();
    }

    private HlsMediaSource buildHlsMediaSource(Uri mp4VideoUri) {
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultHttpDataSourceFactory(
                Util.getUserAgent(this, "VideoApplication"));

        // This is the MediaSource representing the media to be played.
        return new HlsMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mp4VideoUri);
    }

    @Override
    protected void onStart() {
        super.onStart();
        boolean haveStartPosition = (startWindow != C.INDEX_UNSET);
        if (haveStartPosition) {
            exoPlayer.seekTo(startWindow, startPosition);
        }
        exoPlayer.prepare(hlsMediaSource, !haveStartPosition, false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        exoPlayer.stop();
        updateStartPosition();
    }

    @Override
    protected void onPause() {
        super.onPause();
        exoPlayer.setPlayWhenReady(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        exoPlayer.setPlayWhenReady(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (exoPlayer != null) {
            exoPlayer.release();
        }
    }

    private void updateStartPosition() {
        if (exoPlayer != null) {
            startWindow = exoPlayer.getCurrentWindowIndex();
            startPosition = Math.max(0, exoPlayer.getContentPosition());
        }
    }

    private void clearStartPosition() {
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}
