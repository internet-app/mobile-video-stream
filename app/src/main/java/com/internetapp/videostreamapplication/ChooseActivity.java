package com.internetapp.videostreamapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChooseActivity extends Activity {
    String siteIp;
    RecyclerAdapter adapter;

    private RequestAsyncInterface requestAsyncInterface = new RequestAsyncInterface() {
        @Override
        public void onAsyncTaskResult(int code, byte[] msg, int taskId) {
            if (Constant.END_POST_TASK_ID > taskId && taskId >= Constant.START_POST_TASK_ID) {
                processPostResult(code, msg, taskId);
            } else if (Constant.END_GET_TASK_ID > taskId && taskId >= Constant.START_GET_TASK_ID) {
                processGetResult(code, msg, taskId);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        Intent intent = getIntent();
        siteIp = intent.getStringExtra(Constant.SITE_IP_ADDRESS);

        SendPostAsyncTask sendPostAsyncTask = new SendPostAsyncTask(
                getSharedPreferences(Constant.SP_TOKEN_PREF, MODE_PRIVATE)
        );
        sendPostAsyncTask.setOnResultListener(requestAsyncInterface);
        sendPostAsyncTask.setTaskId(Constant.START_POST_TASK_ID);
        String requestUrl = siteIp + "/mobile/video/";
        sendPostAsyncTask.execute(requestUrl, "");
    }

    private void processGetResult(int code, byte[] msg, int taskId) {
        if (code < 200 || 299 < code) {
            Toast.makeText(this, "Error: " + code + " for " + taskId,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        Bitmap bmp = BitmapFactory.decodeByteArray(msg, 0, msg.length);
        if (bmp != null) {
            adapter.updateItemImage(taskId - Constant.START_GET_TASK_ID, bmp);
        }
    }

    private void processPostResult(int code, byte[] msg, int taskId) {
        if (code < 200 || 299 < code) {
            Toast.makeText(this, "Error: " + code + " for " + taskId,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        // Insert images to recycler view
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        List<VideoObject> videoObjectList = getVideoObjectList(msg);

        adapter = new RecyclerAdapter(videoObjectList, siteIp);
        recyclerView.setAdapter(adapter);

        if (videoObjectList == null) {
            return;
        }

        for (int i = 0; i < videoObjectList.size(); i += 1) {
            VideoObject videoObject = videoObjectList.get(i);

            SendGetAsyncTask sendGetAsyncTask = new SendGetAsyncTask();
            sendGetAsyncTask.setOnResultListener(requestAsyncInterface);
            sendGetAsyncTask.setTaskId(i + Constant.START_GET_TASK_ID);
            String request = siteIp + videoObject.imagePath;
            sendGetAsyncTask.execute(request);
        }
    }

    @Nullable
    private List<VideoObject> getVideoObjectList(byte[] jsonMsg) {
        JSONObject jMainObj;
        try {
            jMainObj = new JSONObject(new String(jsonMsg));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        JSONArray jMsgArray;
        try {
            jMsgArray = jMainObj.getJSONArray("msg");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        List<VideoObject> videoObjectList = new ArrayList<>();

        for (int i = 0; i < jMsgArray.length(); i += 1) {
            try {
                JSONObject jobj = jMsgArray.getJSONObject(i);

                VideoObject videoObject = new VideoObject();
                videoObject.imagePath = jobj.getString("imgPath");
                videoObject.caption = jobj.getString("caption");
                videoObject.link = jobj.getString("link");
                videoObject.image = BitmapFactory.decodeResource(getResources(), R.drawable.pic1);

                videoObjectList.add(videoObject);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        return videoObjectList;
    }
}
