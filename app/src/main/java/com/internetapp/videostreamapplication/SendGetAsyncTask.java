package com.internetapp.videostreamapplication;

import android.os.AsyncTask;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SendGetAsyncTask extends AsyncTask<String, Void, Integer> {
    private RequestAsyncInterface listener;
    private byte[] resultMsg = null;
    private int taskId = 0;

    void setOnResultListener(RequestAsyncInterface listener) {
        this.listener = listener;
    }

    void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    @Override
    protected Integer doInBackground(String... params) {
        String requestURL = params[0];

        int resultCode = Constant.START_ERROR_CODE;

        try {
            URL url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            resultCode = conn.getResponseCode();
            resultMsg = toByteArray(conn.getContentLength(), conn.getInputStream());

            conn.disconnect();
        } catch (Exception e) {
            resultMsg = e.getMessage().getBytes();
        }

        return resultCode;
    }

    @Override
    protected void onPostExecute(Integer resultCode) {
        super.onPostExecute(resultCode);
        listener.onAsyncTaskResult(resultCode, resultMsg, taskId);
    }

    @Nullable
    private byte[] toByteArray(int expectedLength, InputStream is) {
        // 104857600 - Theoretical limit is 100 mb
        if (expectedLength > 104857600 || expectedLength < 1) {
            return null;
        }

        byte[] resultArray = new byte[expectedLength];
        int readedBytes = 0;
        int loopResult;
        do {
            try {
                loopResult = is.read(resultArray, readedBytes, expectedLength);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            readedBytes += loopResult;
            expectedLength -= loopResult;
        } while (loopResult != -1 && expectedLength != 0);

        return resultArray;
    }
}
