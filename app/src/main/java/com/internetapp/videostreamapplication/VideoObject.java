package com.internetapp.videostreamapplication;

import android.graphics.Bitmap;

class VideoObject {
    String imagePath;
    Bitmap image;
    String caption;
    String link;
}
