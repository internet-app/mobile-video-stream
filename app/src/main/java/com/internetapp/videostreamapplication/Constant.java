package com.internetapp.videostreamapplication;

final class Constant {
    static final int START_ERROR_CODE = 0;
    static final String NAME_PATTERN = "^(?=.{3,30}$)[a-zA-Z0-9]+$";
    static final String EMAIL_PATTERN = "^(?=.{10,40}$)[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
    static final String SP_TOKEN_PREF = "secret_token";
    static final String SP_TOKEN_NAME = "token";

    // [1024, 65536)
    static final int START_GET_TASK_ID = 1024;
    static final int END_GET_TASK_ID = 65536;

    // [1, 1024)
    static final int START_POST_TASK_ID = 0;
    static final int END_POST_TASK_ID = 1024;

    static final String SITE_IP_ADDRESS = "siteIpAddress";
    static final String VIDEO_STREAM_URL = "VideoStreamUrl";

    static final int TASK_SIGN_IN = 1234;
    static final int TASK_SIGN_UP = 1235;
}
