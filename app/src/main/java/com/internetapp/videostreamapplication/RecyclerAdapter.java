package com.internetapp.videostreamapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ImageViewHolder> {
    private List<VideoObject> videoObjectList;
    private String siteIp;

    RecyclerAdapter(List<VideoObject> videoObjectList, @NonNull String siteIp) {
        this.siteIp = siteIp;
        if (videoObjectList != null)
        {
            this.videoObjectList = videoObjectList;
        } else {
            this.videoObjectList = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_layout, parent, false);

        return new ImageViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        // Make changes here to load image in ImageView
        VideoObject videoObject = videoObjectList.get(position);
        holder.Album.setImageBitmap(videoObject.image);
        holder.AlbumTitle.setText(videoObject.caption);
    }

    @Override
    public int getItemCount() {
        return videoObjectList.size();
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        ImageView Album;
        TextView AlbumTitle;
        RecyclerAdapter adapter;

        ImageViewHolder(View itemView, RecyclerAdapter adapter) {
            super(itemView);
            Album = itemView.findViewById(R.id.item_img);
            AlbumTitle = itemView.findViewById(R.id.item_text);
            itemView.setOnClickListener(this);
            this.adapter = adapter;
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            VideoObject videoObject = adapter.videoObjectList.get(getAdapterPosition());

            String streamUrl = adapter.siteIp + videoObject.link;
            Intent intent = new Intent(v.getContext(), VideoStreamActivity.class);
            intent.putExtra(Constant.VIDEO_STREAM_URL, streamUrl);
            context.startActivity(intent);
        }
    }

    void updateItemImage(int index, Bitmap itemImage) {
        if (videoObjectList.size() <= index && itemImage != null) {
            return;
        }

        VideoObject videoObject = videoObjectList.get(index);
        videoObject.image = itemImage;
        videoObjectList.set(index, videoObject);
        notifyItemChanged(index);

    }
}
