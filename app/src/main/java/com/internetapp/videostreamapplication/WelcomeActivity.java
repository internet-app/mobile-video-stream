package com.internetapp.videostreamapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;

public class WelcomeActivity extends Activity {
    private String siteIp;

    private RequestAsyncInterface requestAsyncInterface = new RequestAsyncInterface() {
        @Override
        public void onAsyncTaskResult(int code, byte[] msg, int taskId) {
            processPostResult(code, msg, taskId);
        }
    };

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.b_welcome_signin:
                    signInButtonClicked();
                    break;
                case R.id.b_welcome_signup:
                    signUpButtonClicked();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Button signInButton = findViewById(R.id.b_welcome_signin);
        Button signUpButton = findViewById(R.id.b_welcome_signup);

        signInButton.setOnClickListener(onClickListener);
        signUpButton.setOnClickListener(onClickListener);
    }

    private void signInButtonClicked() {
        EditText siteIpText = findViewById(R.id.et_welcome_ip);
        siteIp = siteIpText.getText().toString();

        if (siteIp.length() == 0) {
            Toast.makeText(this, "Need to fill ip field", Toast.LENGTH_SHORT).show();
            return;
        }

        SendPostAsyncTask sendPostAsyncTask = new SendPostAsyncTask(
                getSharedPreferences(Constant.SP_TOKEN_PREF, MODE_PRIVATE)
        );
        sendPostAsyncTask.setOnResultListener(requestAsyncInterface);
        sendPostAsyncTask.setTaskId(Constant.TASK_SIGN_IN);
        sendPostAsyncTask.execute(siteIp, "");
    }

    private void signUpButtonClicked() {
        EditText siteIpText = findViewById(R.id.et_welcome_ip);
        siteIp = siteIpText.getText().toString();

        if (siteIp.length() == 0) {
            Toast.makeText(this, "Need to fill ip field", Toast.LENGTH_SHORT).show();
            return;
        }

        SendPostAsyncTask sendPostAsyncTask = new SendPostAsyncTask(
                getSharedPreferences(Constant.SP_TOKEN_PREF, MODE_PRIVATE)
        );
        sendPostAsyncTask.setOnResultListener(requestAsyncInterface);
        sendPostAsyncTask.setTaskId(Constant.TASK_SIGN_UP);
        sendPostAsyncTask.execute(siteIp, "");
    }

    private void processPostResult(int code, byte[] msg, int taskId) {
        String strMessage = new String(msg, StandardCharsets.UTF_8);
        if (code < 200 || 299 < code) {
            if("The token does not found".equals(strMessage)) {
                SendPostAsyncTask sendPostAsyncTask = new SendPostAsyncTask(
                        getSharedPreferences(Constant.SP_TOKEN_PREF, MODE_PRIVATE)
                );
                sendPostAsyncTask.setOnResultListener(requestAsyncInterface);
                sendPostAsyncTask.setTaskId(taskId);
                sendPostAsyncTask.execute(siteIp, "");
            } else {
                Toast.makeText(this, new String(msg, StandardCharsets.UTF_8), Toast.LENGTH_LONG).show();
            }
            return;
        }

        if (!"success".equals(strMessage)) {
            Toast.makeText(this, "Unknown message", Toast.LENGTH_SHORT).show();
            if (taskId == Constant.TASK_SIGN_IN && "authorized".equals(strMessage)) {
                Intent intent;
                intent = new Intent(this, ChooseActivity.class);
                intent.putExtra(Constant.SITE_IP_ADDRESS, siteIp);
                startActivity(intent);
            }
            return;
        }

        Intent intent;
        if (taskId == Constant.TASK_SIGN_UP) {
            intent = new Intent(this, RegisterActivity.class);
        } else if (taskId == Constant.TASK_SIGN_IN) {
            intent = new Intent(this, LoginActivity.class);
        } else {
            return;
        }

        intent.putExtra(Constant.SITE_IP_ADDRESS, siteIp);
        startActivity(intent);
    }
}
